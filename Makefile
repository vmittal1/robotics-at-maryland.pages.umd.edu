IMAGE = ram/website

.PHONY: container
container: Dockerfile
	docker build --tag=$(IMAGE) ./

.PHONY: serve
serve: container
	docker run --user="$$(id -u):$$(id -g)" --name ram-website --rm --net host --detach --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) yarn dev

.PHONY: build
build:
	docker run --user="$$(id -u):$$(id -g)" --name ram-website --rm --net host --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) yarn build

.PHONY: container-shell
container-shell:
	docker run --user="$$(id -u):$$(id -g)" --name ram-website --rm --net host --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) /usr/bin/bash

# DOCKER_RUN = docker run --rm -i --user="$$(id -u):$$(id -g)" --net=none -v "$$PWD":/data:rw $(IMAGE)

