FROM node:lts

EXPOSE 3000/tcp

COPY ./docker/entrypoint.sh /entrypoint.sh

RUN mkdir /website
COPY ./package.json ./yarn.lock /website/.

WORKDIR /website

RUN yarn install
RUN yarn next telemetry disable

ENTRYPOINT ["/entrypoint.sh"]
