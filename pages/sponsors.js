'use client';
import * as React from 'react';
import { Container, Typography, Grid, Button, Divider } from '@mui/material'
import Image from '@/components/Image';
import Email from '@mui/icons-material/Email';
import SEO from '@/components/SEO';

const CompE = {
    src: '/CompElogo.png',
    alt: 'Department of Electrical and Computer Engineering Logo'
}
const SchoolOfEngineering = {
    src: '/image68.png',
    alt: 'A. James Clark School of Engineering Logo'
}
const ComputerScience = {
    src: '/CSLogo.png',
    alt: 'Computer Science Department Logo'
}
const RoboticsCenter = {
    src: '/MRCLogo.png',
    alt: 'Maryland Robotics Center Logo'
}
const RoboticsResearch = {
    src: '/robotic-research-logo.png',
    alt: 'Robotics Research Logo'
}
const SSL = {
    src: '/SSLlogo.png',
    alt: 'Space Systems Lab Logo'
}
const TerrapinWorks = {
    src: '/terrapin-works-logo.png',
    alt: 'Terrapin Works Logo'
}

export default function Sponsors() {
    return (
        <>
            <SEO/>
            <Container maxWidth='false' sx={{ py: 15, backgroundColor:'common.white' }}>
                <Typography variant='h1' textAlign='center' color="common.black" fontWeight={600}>Become a Sponsor Today</Typography>



                <Grid container spacing={3} alignItems='center' textAlign='center' sx={{ pt: 5 }} color='inherit' >

                    <Grid item sm={2}></Grid>
                    
                    <Grid item xs={12} sm={4} justifyContent='center'>
                        <Button href="/ram_sponsor_packet_2023_2024.pdf" download="ram_sponsor_packet" variant='contained'>
                            <Typography variant='h6'> Sponsor Packet</Typography>
                        </Button>
                    </Grid>
                    <Grid item xs={12} sm={4} justifyContent='center'>
                        <Button href='mailto:team@ram.umd.edu' startIcon={<Email />} variant='contained'>
                            <Typography variant='h6'>Contact Us</Typography>
                        </Button>
                    </Grid>

                    <Grid item sm={2}></Grid>


                    <Grid item md={2}></Grid>
                    <Grid item xs={12} md={8} alignItems='center'>
                        <Typography paragraph variant='h5' color="grey.800" sx={{p:3}}> We are always welcome to new sponsors with contributions of any size or form. By becoming a sponsor you will become our mentor too! We host events throughout the semester to meet with corporate partners and discuss our designs. Collect our resumes, meet the team, and get your name front and center on UMD’s largest and most vibrant robotics club! </Typography>
                    </Grid>
                    <Grid item md={2}></Grid>

                    <Grid item md={2}></Grid>
                    <Grid item xs={12} md={8} alignItems='center'>
                        <Typography paragraph color="grey.800" > Gifts in support of the University of Maryland are accepted and managed by the University of Maryland College Park Foundation, Inc. an affiliated 501(c)(3) organization authorized by the Board of Regents. Contributions to the University of Maryland are tax deductible as allowed by law.
Please see your tax advisor for more details. </Typography>
                    </Grid>
                    <Grid item md={2}></Grid>

                </Grid>

            </Container>

            <Container maxWidth='false' sx={{py:10, backgroundColor:'primary.main'}}>
                <Typography variant='h2' textAlign='center' fontWeight={600} sx={{py:3}}> Thank You To Our Sponsors</Typography>
            </Container>

            <Container maxWidth='false'  sx={{ py:10, backgroundColor:'common.white'}}>
                <Grid item xs={12} md={4} >
                    <Typography paragraph variant='h6' textAlign='left' color='common.black' sx={{p:5}}> Robotics at Maryland’s success is solely reliant on the invaluable contributions of our generous sponsors. We would like to thank all of them for allowing us to not only to prepare our members for a career in robotics, but also to foster a love of collaboration, challenge, and learning.</Typography>
                </Grid>
                <Grid container spacing={2}  backgroundColor='common.white' justifyContent='center' alignItems='flex-start'>
                    <Grid container item xs={12} md={8}>
                        <Grid container item xs={9} md={5} >
                            <Grid item xs={12} >
                                <Image src={SchoolOfEngineering.src} alt={SchoolOfEngineering.alt} height={150} backgroundSize='contain' />
                            </Grid>
                            <Grid item xs={12} >
                                <Image src={CompE.src} alt={CompE.alt} height={100} backgroundSize='contain' />
                            </Grid>
                        </Grid>
                        <Grid item xs={3}>
                            <Image src={SSL.src} alt={SSL.alt} height={300} backgroundSize='contain'/>
                        </Grid>
                        <Grid container item xs={12} md={4} spacing={3} justifyContent='center' alignItems='center'>
                            <Grid item xs='auto' minWidth={250}>
                                <Image src={ComputerScience.src} alt={ComputerScience.alt} height={100} backgroundSize='contain' />
                            </Grid>
                            <Grid item xs='auto' minWidth={250}>
                                <Image src={RoboticsCenter.src} alt={RoboticsCenter.alt} height={100} backgroundSize='contain' />
                            </Grid>
                            <Grid item xs='auto' minWidth={250}>
                                <Image src={RoboticsResearch.src} alt={RoboticsResearch.alt} height={100} backgroundSize='contain' />
                            </Grid>
                        </Grid>
                        <Grid container item xs={12} md={4} spacing={3} justifyContent='center' alignItems='center'>
                            <Grid item xs='auto' minWidth={250}>
                                <Image src={TerrapinWorks.src} alt={TerrapinWorks.alt} height={100} backgroundSize='contain' />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>

        </>
    )
}
