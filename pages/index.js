'use client';

import { Container, Box, Typography, Button, Grid, Link} from "@mui/material";
import Image from '@/components/Image';
import SEO from "@/components/SEO";
import { useTheme } from "@emotion/react";
import YouTube from "react-youtube";

const HeroShot = {
    src: '/FadedNBRF.png',
    alt: 'NBRF Pool'
};
const QuboShot = {
    src: '/FullSizeRender.jpg',
    alt: 'Qubo Robot'
};
const QuboOnTable = {
    src:'/Qubo2.jpg',
    alt:'Qubo On Table'
};
const NBRFWindow = {
    src:'/NBRFWindow.jpg',
    alt:'NBRF Window'
};
const TeamSelfie = {
    src:'/TeamSelfie.jpg',
    alt:'Team Selfie'
};
const CNC = {
    src:'/CNC.jpg',
    alt:'CNC Machine'
};
const TestudogAssembling = {
    src:'/TestudogAssembiling.jpg',
    alt:'Testudog being assembeled'
};
const IdeaStepsImage = {
    src:'/IdeaSteps.png',
    alt:'Idea Factory Steps'
};
const TestudogShot = {
    src:'/TestudogAssembeled.png',
    alt:'Testudog Robot'
}

const Printer1 = {
    src: '/3DPrinter.jpg',
    alt:'3D Printer'
}
const Printer2 = {
    src: '/3DPrinter2.jpg',
    alt:'3D Printer'
}

const BuildingInNBRF = {
    src: '/NBRFBuilding.jpg',
    alt: 'The team building at NBRF'
}
// home page
export default function Index() {

    const theme = useTheme();
    return (
        <>
            <SEO/>

            {/*<Box

                sx={{
                    backgroundColor:"primary.main",
                    width:1,
                    py:2,
                }}
            >
                <Container
                    maxWidth="lg"
                    sx={{
                        backgroundColor:"primary.main",

                    }}
                >

                    <Grid item xs={12} sm={6} md={3} minHeight={200}
                            sx={{display:'flex', justifyContent:'space-between', flexDirection:'row', p:4,

                            [theme.breakpoints.down('sm')]: {
                                flexDirection:'column'

                              },
                        }}
                        >
                    <Container>
                    <Typography
                                            variant='h3'
                                            textAlign='center'
                                            fontWeight={600}

                                            sx = {{
                                                [theme.breakpoints.down('sm')]: {
                                                    variant: "h4"

                                                  },
                                            }}>
                        Giving Tuesday
                    </Typography>
                    <Typography variant='h6'
                                            textAlign='center'
                                            fontWeight={600}

                                            sx = {{
                                                [theme.breakpoints.down('sm')]: {
                                                    fontSize: "15px",
                                                  },
                                            }}>
                        Fundraiser Begins November 28th
                    </Typography>
                    <Box paddingTop = "10px" textAlign = 'center'
                        sx = {{ [theme.breakpoints.down('sm')]: {fontSize: "15px",paddingBottom: "15px"}}}>
                        <Button sx = {{color: "white", backgroundColor: "#000000"}} href = "https://launch.umd.edu/project/39982">
                            <Typography fontSize = "20px" >
                                Donate
                            </Typography>
                        </Button>
                    </Box>
                    </Container>

                    <Container>
                    <Typography
                        variant='subtitle1'
                        textAlign='justify'
                        textjustify = "inter-word"
                        fontWeight={600}
                        sx = {{
                            [theme.breakpoints.down('sm')]: {
                                fontSize: "13px"

                              },
                        }}
                    >
                        It&apos;s Giving Tuesday! Giving Tuesday is the University of Maryland&apos;s annual student organization fundraiser. Help sponsor us to compete in 2024&apos;s RoboSub competition! Our goal this year is to bring as many new members as we can, to give them the incredible experience of fighting for the crown in an autonomous underwater challenge. By donating, you&apos;re helping R@M foster a passion for robotics in more students.
                    </Typography>
                    </Container>
                    </Grid>

                </Container>

            </Box>
             */}


            {/* splash screen */}
            <Box
                sx={{
                    backgroundImage: `url(${HeroShot.src})`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    height: {xs:600, md:900},
                    width:1,
                    py:10,
                    title:HeroShot.alt,
                    display: 'flex',
                    justifyContent: 'center'
                }}
            >
                <Container maxWidth='lg' 
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-evenly'
                    }}
                >
                    {/* Eye catching headline */}
                    <Typography variant='h1' textAlign='center' color='common.white' sx={{ fontWeight: 800 }}>Pioneering the future of autonmous systems.</Typography>
                    
                    {/* Action Button */}
                    {/* To Do separate into respective user type actions ie. sponsors new members */}
                    <Button
                        
                        variant='contained'
                        
                        
                        color='primary'

                        href='#calltoaction'
                        sx={{
                            width:250,
                            height:50,
                            alignSelf:'center',
                            textAlign:'center',
                            p:2

                        }}

                    >
                        <Typography variant='h5' fontWeight='800' color='commmon.white'>Get Involved</Typography>
                    </Button>
                    <Box>
                        <Typography variant='h3' textAlign='center' color='commmon.white'>University of Maryland</Typography>
                        <Typography variant='h4' textAlign='center' color='commmon.white'>Premier Student Robotics Club</Typography>
                    </Box>
                </Container>
                
            </Box>
            {/* MEETING INFO */}
            <Box
                
                sx={{
                    mb:10,
                    py:10,
                    px:5,
                    backgroundColor:"primary.main",
                    width:1
                }}
            >
                <Container
                    maxWidth="xl"
                    sx={{
                        backgroundColor:"primary.main"
                    }}
                >
                    <Typography
                        variant='h2'
                        textAlign='left'
                        fontWeight={600}
                        sx={{
                            pb:5
                        }}
                    >
                        Meetings:
                    </Typography>
                    
                   
                    <Grid
                        container
                        spacing={3}
                    >
                    
                        <Grid item xs={12} sm={6} md={3} minHeight={200} 
                            sx={{display:'flex', justifyContent:'space-between', flexDirection:'column', p:4}}
                        >
                            <Typography
                                variant='body1'
                                fontSize={30}
                                fontWeight={600}
                            >
                                Catamaran:
                            </Typography>
                            <Box>
                                <Typography variant='body1' fontSize={22} color="ternary.main" fontWeight={600}> For New Members </Typography>
                                <Link href="https://maps.app.goo.gl/6ZvoLeFEN2szKE5z7" color="inherit" variant='body1' fontSize={{xs:20, md:22}}> 
                                    Leidos Lab, A. James Clark Hall
                                </Link>
                            </Box>
                            <Box>
                                <Typography variant='body1' fontSize={22}> <b>Wednesdays:</b> 7-9 PM</Typography>
                                <Typography variant='body1' fontSize={22}> <b>Sundays:</b> 1-3 PM</Typography>
                            </Box>
                            
                        </Grid>

                        <Grid item xs={12} sm={6} md={3} minHeight={200} 
                            sx={{display:'flex', justifyContent:'space-between', flexDirection:'column', p:4}}
                        >
                            
                            <Typography
                                variant='body1'
                                fontSize={30}
                                fontWeight={600}
                            >
                                Electrical:
                            </Typography>
                        
                        
                            <Link href="https://maps.app.goo.gl/HmuyTvrGQpBV4tbT8" color="inherit" variant='body1' fontSize={{xs:20, md:22}}> 
                                Robotics and Autonomy Lab, E.A. Fernandez IDEA Factory
                            </Link>
                            <Box>
                                <Typography variant='body1' fontSize={22}> <b>Wednesdays:</b> 7-9 PM</Typography>
                                <Typography variant='body1' fontSize={22}> <b>Sundays:</b> 1-3 PM</Typography>
                            </Box>
                            
                        </Grid>
                        <Grid item xs={12} sm={6} md={3} minHeight={200} 
                            sx={{display:'flex', justifyContent:'space-between', flexDirection:'column', p:4}}
                        >
                            
                            <Typography
                                variant='body1'
                                fontSize={30}
                                fontWeight={600}
                            >
                                Mechanical:
                            </Typography>
                            <Link href="https://maps.app.goo.gl/HmuyTvrGQpBV4tbT8" color="inherit" variant='body1' fontSize={{xs:20, md:22}}  > 
                                Robotics and Autonomy Lab, E.A. Fernandez IDEA Factory
                            </Link>
                            <Box>
                                <Typography variant='body1' fontSize={22}> <b>Wednesdays:</b> 7-9 PM</Typography>
                                <Typography variant='body1' fontSize={22}> <b>Sundays:</b> 1-3 PM</Typography>
                            </Box>
                            
                            
                        </Grid>
                        <Grid item xs={12} sm={6} md={3} minHeight={200} 
                            sx={{display:'flex', justifyContent:'space-between', flexDirection:'column', p:4}}
                        >
                            
                            <Typography
                                variant='body1'
                                fontSize={30}
                                fontWeight={600}
                            >
                                Software:
                            </Typography>
                            <Link href="https://maps.app.goo.gl/SD9TbB27zyVw1A4D6" color="inherit" variant='body1' fontSize={{xs:20, md:22}}> 
                                Neutral Buoyancy Research Facility
                            </Link>
                            <Box>
                                <Typography variant='body1' fontSize={22}> <b>Wednesdays:</b> 7-9 PM</Typography>
                                <Typography variant='body1' fontSize={22}> <b>Sundays:</b> 1-3 PM</Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Container>
            </Box>

            {/* Mission statement */}
            <Container
                maxWidth='xl'
                sx={{
                    
                    my:10
                }}
            >
                <Typography
                    variant='body1'
                    textAlign='center'
                    color='grey.200'
                    fontSize={{xs:35, md:45}}
                    sx={{
                        fontWeight: 600
                    }}
                    paragraph
                >
                    {/* OLD: We are a diverse multi-disciplinary team of undergraduate students collaborating on solving real-world challenges and gaining hands-on experience in the process. */}
                    Robotics @ Maryland (R@M) is a student-run organization dedicated to furthering robotics education through the hands-on development of multiple robotic systems.
                    {/* NEW TOO LONG: Robotics @ Maryland (R@M) is a student-run organization dedicated to furthering robotics education through the hands-on development of multiple robotic systems. Every year, we send our autonomous underwater vehicle (AUV) to compete in the RoboNation RoboSub competition in San Diego. We are expanding the scope of our club to build a quadruped robot dog, incorporate a sister organization, Terraformers, to develop a Mars Rover, and create an onboarding program to incorporate new members into the complex field of robotics. R@M cultivates a holistic understanding of robotics, encompassing design, fabrication, assembly, and testing, equipping members for real-world engineering challenges. */}
                </Typography>
            </Container>



            

            {/* Second info Module */}
            {/* Club Details */}
            <Container
                maxWidth='xl'
                sx={{
                    my:12
                }}
                
            >
                <Grid
                    container
                    spacing={3}
                    textAlign='center'
                    sx={{
                        py: 4
                    }}
                >
                    <Grid container item xs={12} md={6} spacing={3} minHeight={{xs:600, md:900}}>
                        <Grid item xs={12}>
                            <Image src={TeamSelfie.src} alt={TeamSelfie.alt}/>
                            
                        </Grid>
                        <Grid item xs={12} >
                            <Image src={QuboOnTable.src} alt={QuboOnTable.alt} />
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={6} minHeight={{xs:300, md:900}}>
                        <Image src={NBRFWindow.src} alt={NBRFWindow.alt} />
                    </Grid>

                    <Grid item xs={6} md={3} minHeight={500}>
                        <Image src={CNC.src} alt={CNC.alt}/>
                    </Grid>
                    <Grid container item xs={6} md={3} spacing={3} minHeight={500}>
                        
                        <Grid item xs={12}>
                            <Image src={Printer1.src} alt={Printer1.alt}/>
                        </Grid>
                        <Grid item xs={12}>
                            <Image src={Printer2.src} alt={Printer2.alt}/>
                        </Grid>
                        
                    </Grid>

                    <Grid item xs={12} md={6} minHeight={{xs:300, md:500}} >
                        <Image src={TestudogAssembling.src} alt={TestudogAssembling.alt} />
                    </Grid>

                    

                </Grid>
                <YouTube 
                    videoId="nyAlDN70oRc" 
                    opts={{
                        height: "600",
                        width: "100%"
                    }}
                />
            </Container>

            <Container maxWidth='xl'>
                
            </Container>

            {/* Robots section */}
            <Box
                sx={{
                    backgroundColor:'primary.main',
                    width:1,
                    heigth:1,
                    mt:15
                }}
            >

            
                <Container
                    maxWidth='xl'
                    sx={{
                        backgroundColor:'primary.main',
                        py:{xs:10, md:15}
                    }}
                    
                >
                    <Grid
                        container
                        spacing={{xs:10, md:3}}
                        rowSpacing={{xs:5, md:30}}
                        textAlign='center'
                    >
                        <Grid item xs={12} md={6} minHeight={{xs:400, md:800}}>
                            <Image src={QuboShot.src} alt={QuboShot.alt}/>
                        </Grid>
                        <Grid container item xs={12} md={6} spacing={5} alignItems='center'>
                            <Container maxWidth='sm' sx={{mx:5, width:1, height:1, display:'flex', flexDirection:'column', justifyContent:'space-around', alignItems:'left', textAlign:'left'}}>
                                <Typography
                                    variant='h1'
                                    color='common.white'
                                    fontWeight={800}
                                    sx={{my:1, py:1}}
                                >
                                    Qubo
                                </Typography>

                                <Typography
                                    variant='h5'
                                    color='common.white'
                                    sx={{my:1, py:1}}
                                >
                                    {/* Dive into Qubo, our second generation competitive autonomous underwater vehicle (AUV) pushing the boundaries of maritime robotics. Our club was founded around competitng in the <span style={{color: theme.palette.ternary.main, fontWeight:800}}>RoboSub</span> competition and it is our largest and longest project to-date. */}
                                    Dive into Qubo, our entrant for the <span style={{color: theme.palette.ternary.main, fontWeight:800}}>2023 RoboSub competition</span>. It is our second generation autonomous underwater vehicle (AUV) pushing the boundaries of maritime robotics. Robotics at Maryland&apos;s largest and longest project to-date.
                                </Typography>
                                <Button size='large' variant='contained' color='ternary' href='/robots/qubo' sx={{width:300, my:1, py:1}}>
                                    <Typography variant='h5' color='common.black' >Learn More</Typography>
                                </Button>
                            </Container>
                        </Grid>
                        
                        
                        {/* UNCOMMENT WHEN DONE WITH TESTUDOG PAGE */}
                        {/* 
                        <Grid item xs={12} md={6} minHeight={{xs:400, md:800}} >
                            <Image src={TestudogShot.src} alt={TestudogShot.alt}/>
                        </Grid> 
                        <Grid container item xs={12} md={6} alignItems='center'>
                            <Container maxWidth='sm' sx={{mx:5, width:1, height:1, display:'flex', flexDirection:'column', justifyContent:'space-around', alignItems:'left', textAlign:'left'}}>
                                <Typography
                                    variant='h1'
                                   
                                    color='commmon.white'
                                    fontWeight={800}
                                    sx={{my:1, py:1}}
                                >
                                    Testudog
                                </Typography>

                                <Typography
                                    variant='h5'
                                    color='commmon.white'
                                    sx={{my:1, py:1}}
                                >
                                    Set forth with Testudog, Robotics at Maryland’s latest project and <span style={{color: theme.palette.ternary.main, fontWeight:800}}> University of Maryland’s first quadrupedal robot</span>. Testudog is an all terrain research and development platform. 
                                </Typography>
                                <Button size='large' variant='contained' color='ternary' href='/robots/testudog' sx={{width:300, my:1, py:1}}>
                                    <Typography variant='h5' color='common.black' >Learn More</Typography>
                                </Button>
                            </Container>
                        </Grid>
                         */}

                    </Grid>

                </Container>
            </Box>

            {/* Call to Action */}
            <Container
                id='calltoaction'
                maxWidth='false'
                sx={{
                    backgroundImage: `url(${IdeaStepsImage.src})`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    height:{xs:1200, lg:770},
                    display:'flex',
                    flexDirection:'column',
                    justifyContent:'space-evenly',
                    title:IdeaStepsImage.alt,
                    py:8
                }}

            >
                <Typography variant='h2' textAlign='center' color='commmon.white' fontWeight={800}>Get Involved</Typography>
                <Grid container textAlign='center' spacing={10} >

                    <Grid item xs={12} md={4} >
                        <Container maxWidth='xs' sx={{display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'space-between', height:1, maxHeight:350}}>
                            <Typography variant='h4' color='commmon.white' fontWeight={600} sx={{py:3}}>For Sponsors</Typography>
                            <Typography paragraph color='commmon.white' textAlign='left' fontSize={18} sx={{py:3}}>Interested in supporting our work? Robotics at Maryland is actively looking for sponsors to help us fund and develop new projects, and to support us as we compete in the 2023 International Robosub competition.</Typography>
                            <Button size='large' variant='contained' href='/sponsors'> <Typography variant='h6'>Sponsor Us</Typography></Button>
                        </Container>
                        {/* <Grid item xs={12} sx={{py:3}}>
                            <Typography variant='h4' color='commmon.white'>For Sponsors</Typography>
                        </Grid>
                        <Grid item xs={12} sx={{py:3}}>
                            <Typography paragraph color='commmon.white' textAlign='left'>Interested in supporting our work? Robotics at Maryland is actively looking for sponsors to help us fund and develop new projects, and to support us as we compete in the 2023 International Robosub competition.</Typography>
                        </Grid>
                        <Grid item xs={12} >
                            <Button size='large' variant='contained' href='/sponsors'> <Typography variant='h6'>Sponsor Us</Typography></Button>
                        </Grid> */}
                        
                    </Grid>

                    <Grid item xs={12} md={4} >
                        <Container maxWidth='xs' sx={{display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'space-between', height:1, maxHeight:350}}>
                            <Typography variant='h4' color='commmon.white' fontWeight={600} sx={{py:3}}>For New Members</Typography>
                            <Typography paragraph color='commmon.white' textAlign='left' fontSize={18} sx={{py:3}}> Want to build robots? We are open to students of all majors and skill levels, and welcome students with or without past technical experience </Typography>
            <Button size='large' variant='contained' href='https://go.umd.edu/ramslack'><Typography variant='h6' color='commmon.white'>Join Us</Typography></Button>
                        </Container>
                        {/* <Grid item xs={12} sx={{py:3}}>
                            <Typography variant='h4' color='commmon.white'>For New Members</Typography>
                        </Grid>
                        <Grid item xs={12} sx={{py:3}}>
                            <Typography paragraph color='commmon.white' textAlign='left'> Want to build robots? We are open to students of all majors and skill levels, and welcome students with or without past technical experience </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Button size='large' variant='contained'><Typography variant='h6' color='commmon.white'>Join Us</Typography></Button>
                        </Grid> */}
                        
                    </Grid>

                    {<Grid  item xs={12} md={4}>
                        <Container maxWidth='xs' sx={{display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'space-between', height:1, maxHeight:350}}>
                            <Typography variant='h4' sx={{m:3}} fontWeight={600} color='commmon.white'>Join Our Newsletter</Typography>
                     <Typography paragraph color='common.white' textAlign='left' fontSize={18} sx={{py:3}}>We send out periodic emails about what we&apos;re up to, interesting events, and more. You can also choose to receive email reminders about meeting times.
                     </Typography>
                            <Button size='large' variant='contained' href='/subscribe'><Typography variant='h6' color='commmon.white'>Subscribe</Typography></Button>
                        </Container>
                    </Grid>
                    }
                </Grid>
            </Container>
        </>

    )

}
