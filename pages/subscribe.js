'use client';

import { Typography, Grid, Container, Button, IconButton } from "@mui/material";
import { useTheme } from "@emotion/react";
import {Newspaper, Email} from '@mui/icons-material';
import SEO from "@/components/SEO";
import SlackIcon from "@/components/SlackIcon";


export default function Subscribe() {
    const theme = useTheme();
    return (
        <>
            <SEO/>
            
            <Container maxWidth='false' sx={{ py:{xs:8, md:20}, backgroundColor:'common.white' }}>
            
                <Typography variant='h1' textAlign='center' color="common.black"  fontWeight={600} sx={{p:3}}>
                     Become A Member
                </Typography>
                
                <Grid container spacing={3} alignItems='center' textAlign='center' sx={{ py: 5}} color='inherit' >

                    <Grid item sm={3}></Grid>
                    <Grid item xs={12} sm={2} justifyContent='center'>
                        <Button href='https://groups.google.com/a/ram.umd.edu/g/newsletter/about' variant='contained' startIcon={<Newspaper/>}><Typography variant='h6' fontSize={20} fontWeight={600}> Newsletter</Typography></Button>
                    </Grid>
                    <Grid item xs={12} sm={2} justifyContent='center'>
                        {/* <Button href='https://groups.google.com/a/ram.umd.edu/g/meeting-reminders/about' variant='contained'><Typography variant='h6'> </Typography></Button> */}
                        <IconButton href='https://go.umd.edu/ramslack'><SlackIcon   sx={{fontSize:150, color:"common.black"}} /></IconButton>
                    </Grid>
                    <Grid item xs={12} sm={2} justifyContent='center'>
                        <Button href='https://groups.google.com/a/ram.umd.edu/g/meeting-reminders/about' startIcon={<Email />} variant='contained'><Typography variant='h6' fontSize={20} fontWeight={600} >Meetings</Typography></Button>
                    </Grid>
                    <Grid item sm={3}></Grid>

                    
                    <Grid item xs={12}>
                        <Typography href='/non-gmail-subscribe' variant='caption' component='a' color='common.black' textAlign='center'>For Non-Gmail Accounts</Typography>
                    </Grid>
                    
                    
                    
                </Grid>


            </Container>
        </>
    );
}
