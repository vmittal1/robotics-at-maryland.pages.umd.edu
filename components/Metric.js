'use client';
import * as React from 'react';
import {Typography, Box} from '@mui/material';
export default function Metric(props){
    return (
        <Box
            sx={props.sx}
        >
            <Typography variant='h2' sx={{fontWeight:800}} color={props.color}>{props.metric}</Typography>
            <Typography variant='h5' color={props.color}>{props.subtitle}</Typography>
        </Box> 
    )
}